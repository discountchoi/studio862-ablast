﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour {
    // Start is called before the first frame update
    public float spawnMin;
    public float spawnRate;
    public int spawnLevel;
    public int index;

    private float timeAfterSpawn;
    private GameObject tempObj;
    public GameObject[] gameObjects;

    void Start() {
        timeAfterSpawn = 0f;
    }

    // Update is called once per frame
    void Update() {
        if (GameManager.instance.isGameover()) {
            if (tempObj != null) {
                Destroy(tempObj);
            }
        }
        int curLevel = GameManager.instance.GetCurrentLevel();
        if (curLevel >= spawnLevel) {
            timeAfterSpawn += Time.deltaTime;
            if (timeAfterSpawn > spawnRate && !GameManager.instance.isGameover()) {
                timeAfterSpawn = 0f;
                Vector3 v = transform.position;
                v.y = Random.Range(-4.6f, 4.6f);

                var x = Random.Range(-1.0f, 1.0f);
                if(x>0) {
                    v.x = 10f;
                } else {
                    v.x = -10f;
                }

                tempObj = Instantiate(gameObjects[index], v, Quaternion.identity);
                spawnRate = spawnRate - (spawnRate * ((curLevel - spawnLevel) * (0.5f / 10f)));
                spawnRate = Mathf.Max(spawnMin, spawnRate);                
            }
        }
    }
}
