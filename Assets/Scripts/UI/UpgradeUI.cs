﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Feature {
    public string name;
    public Sprite image;
    public Feature(string name, Sprite image) {
        this.name = name;
        this.image = image;
    }
}

public class UpgradeUI : MonoBehaviour
{
    public GameObject[] features;

    List<Feature> index;
    // Start is called before the first frame update
    private void OnEnable() {
        SoundManager.instance.PlaySound(SoundManager.Sounds.Upgrade);
        if (!GameManager.instance.character.GetComponent<PlayerAction>().HasShield() && !GameManager.instance.items.ContainsKey("shield")) {
            GameManager.instance.items.Add(DataManager.instance.shield.name, DataManager.instance.shield.sprite);
        }

        if(GameManager.instance.character.GetComponent<PlayerAction>().HasPet() && !GameManager.instance.items.ContainsKey("petAttackSpeed")) {
            GameManager.instance.items.Add(DataManager.instance.petAttackSpeed.name, DataManager.instance.petAttackSpeed.sprite);
        }

        DrawLotsFromDict();
    }
    
    public void DrawLotsFromDict() {
        index = new List<Feature>();

        foreach (var kv in GameManager.instance.items) {
            index.Add(new Feature(kv.Key, kv.Value));
        }

        for (int i = 0; i < features.Length; i++) {
            var picked = Random.Range(0, index.Count);
            SetFeature(i, picked);
            index.RemoveAt(picked);
        }
    }

    void SetFeature(int dest, int src) {
        features[dest].GetComponent<Image>().sprite = index[src].image;
        features[dest].GetComponentInChildren<Text>().text = index[src].name;
    }
}
