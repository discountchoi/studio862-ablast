﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{
    public GameObject LeftButton;
    public GameObject RightButton;
    public SpriteRenderer chracterRenderer;

    private void Start() {
        chracterRenderer.sprite = DataManager.instance.characters[DataManager.instance.index].GetComponent<SpriteRenderer>().sprite;
    }

    public void Play() {
        if (DataManager.instance.loaded) {
            SoundManager.instance.PlaySound(SoundManager.Sounds.Menu);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    public void Rank() {
        SoundManager.instance.PlaySound(SoundManager.Sounds.Menu);
        GoogleManager.Instance.ShowRank();
    }

    public void OpenHelp() {
        SoundManager.instance.PlaySound(SoundManager.Sounds.Menu);
        DataManager.instance.GetHelpPanel().SetActive(true);
    }

    public void CloseHelp() {
        SoundManager.instance.PlaySound(SoundManager.Sounds.Menu);
        DataManager.instance.GetHelpPanel().SetActive(false);
    }

    public void Quit() {
        SoundManager.instance.PlaySound(SoundManager.Sounds.Menu);
        Application.Quit();
    }

    public void Left() {
        var animator = LeftButton.GetComponent<Animator>();
        animator.SetTrigger("click");
        DataManager.instance.index = (DataManager.instance.index + DataManager.instance.characters.Length - 1) % DataManager.instance.characters.Length;
        print(DataManager.instance.index);
        chracterRenderer.sprite = DataManager.instance.characters[DataManager.instance.index].GetComponent<SpriteRenderer>().sprite;
    }

    public void Right() {
        var animator = RightButton.GetComponent<Animator>();
        animator.SetTrigger("click");
        DataManager.instance.index = (DataManager.instance.index + 1) % DataManager.instance.characters.Length;
        print(DataManager.instance.index);
        chracterRenderer.sprite = DataManager.instance.characters[DataManager.instance.index].GetComponent<SpriteRenderer>().sprite;
    }
}
