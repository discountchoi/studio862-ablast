﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LevelSystem : MonoBehaviour {
    // Start is called before the first frame update
    public int ExpForLevelUp;
    public UnityEvent OnLevelUp;
    private int currentExp = 0;
    private Text levelText;
    private int currentLevel = 1;
    private Image progessImage;
    private float normalizedExp;

    void Awake() {
        levelText = transform.Find("LevelText").GetComponent<Text>();
        progessImage = transform.Find("Exp").Find("Frame").Find("Progress").GetComponent<Image>();
        OnLevelUp = new UnityEvent();
    }

    void Start() {
        OnLevelUp.AddListener(GameManager.instance.LoadNextStage);
        //OnLevelUp.AddListener(GameManager.instance.ActivateUpgradeUI);
    }

    public int GetCurrentLevel() {
        return currentLevel;
    }

    public void AddExp(int exp) {
        currentExp += exp;
        if (currentExp >= ExpForLevelUp) {
            ++currentLevel;
            levelText.text = "STAGE Level : " + currentLevel;
            UpdateProgressBar(1);
            currentExp = 0;
            // for debug
            ExpForLevelUp *= 2;
            OnLevelUp.Invoke();
        }

        normalizedExp = (float)currentExp / ExpForLevelUp;
        UpdateProgressBar(normalizedExp);
    }

    void UpdateProgressBar(float value) {
        progessImage.fillAmount = value;
    }
}
