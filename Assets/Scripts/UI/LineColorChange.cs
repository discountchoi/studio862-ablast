﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineColorChange : MonoBehaviour
{
    // Start is called before the first frame update
    public Color color;
    public LineRenderer lineRenderer;

    // Update is called once per frame
    void Update()
    {
        var col = Color.Lerp(Color.white, color, Mathf.PingPong(Time.time, 1));
        lineRenderer.startColor = col;
        lineRenderer.endColor = col;
    }
}
