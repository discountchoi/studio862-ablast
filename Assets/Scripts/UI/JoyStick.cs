﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoyStick : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IDragHandler {
    // Start is called before the first frame update
    private RectTransform panel;
    private RectTransform stick;
    private float radius;
    private bool isTouch = false;
    private IPlayer player;
    private Vector2 pos;
    public GameObject joyStickOutCircle;
    public GameObject joyStickInCircle;
    private Vector3 joyStickFirstPosition;
    void Start() {
        panel = joyStickOutCircle.GetComponent<RectTransform>();
        stick = joyStickInCircle.GetComponent<RectTransform>();
        radius = panel.rect.width * 0.5f;
        player = GameManager.instance.character.GetComponent<IPlayer>();
    }

    // Update is called once per frame
    void Update() {
        if (isTouch) {
            player.Move(pos);
        }
    }

    public void OnDrag(PointerEventData eventData) {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(panel, eventData.position, eventData.pressEventCamera, out pos);
        pos -= (Vector2)joyStickInCircle.transform.position;
        pos = Vector2.ClampMagnitude(pos, radius);
        joyStickInCircle.transform.localPosition = pos;
    }

    public void OnPointerDown(PointerEventData eventData) {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(transform.GetComponent<RectTransform>(), eventData.position, eventData.pressEventCamera, out pos);
        joyStickFirstPosition = joyStickOutCircle.transform.localPosition;
        joyStickOutCircle.transform.localPosition = pos;
        joyStickInCircle.transform.localPosition = Vector3.zero;
        isTouch = true;
        
    }

    public void OnPointerUp(PointerEventData eventData) {
        joyStickOutCircle.transform.localPosition = joyStickFirstPosition;
        joyStickInCircle.transform.localPosition = Vector3.zero;
        isTouch = false;
    }
}

