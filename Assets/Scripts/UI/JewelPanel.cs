﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JewelPanel : MonoBehaviour
{
    // Start is called before the first frame update
    static public JewelPanel instance;
    public Text jewelText;

    private void Awake() {
        if(instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    public void JewelTextUpdate(int jewelCnt) {
        jewelText.text = jewelCnt.ToString();
    }
}
