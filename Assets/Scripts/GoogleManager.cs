﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class GoogleManager : MonoBehaviour {
    // Start is called before the first frame update
    static private GoogleManager instance;

    public static GoogleManager Instance {
        get {
            if(instance == null) {
                instance = new GameObject("Google Play Service").AddComponent<GoogleManager>();
            }

            return instance;
        }
    }

    public bool isAuthenticated {
        get {
            return Social.localUser.authenticated;
        }
    }
    private void Awake() {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().EnableSavedGames().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = false;
        PlayGamesPlatform.Activate();
    }

    public void Login() {
        Social.localUser.Authenticate((bool success) => {
            if(!success) {
                Debug.Log("login fail");
            }
        });
    }

    public void ShowRank() {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_score);
    }
}

