﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerMovement : MonoBehaviour, IPlayer {
    // Start is called before the first frame update
    public float speed;
    public GameObject explode;
    public PolygonCollider2D polygonCollider2D;
    public bool anked = false;
    public void Die() {
        SoundManager.instance.PlaySound(SoundManager.Sounds.PlayerDie);
        GameManager.instance.OnPlayerDead();
        Instantiate(explode, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if ((collision.tag == "Enemy" || collision.tag == "Wall" || collision.tag == "EnemyBullet") 
            && polygonCollider2D.IsTouching(collision)) {
            Die();
        } else if (collision.tag == "Item" && polygonCollider2D.IsTouching(collision)) {
            IItem item = (IItem)collision.GetComponent(typeof(IItem));
            item.Use(gameObject);
        }
    }

    public void Move(Vector2 pos) {
        var angle = Vector2.SignedAngle(Vector2.right, pos);
        // Make up to world angle.
        transform.rotation = Quaternion.Euler(0, 0, angle - 90);
        if (!anked) {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }
    }
}

