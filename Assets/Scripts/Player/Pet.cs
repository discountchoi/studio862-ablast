﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pet : MonoBehaviour {
    public GameObject missile;
    private bool possiblity;

    // Start is called before the first frame update
    void Start() {
        possiblity = true;
    }

    // Update is called once per frame
    void Update() {
        if (possiblity) {
            var missileObj = Instantiate(missile, transform.position, transform.rotation);
            StartCoroutine("ActionDelay");
        }
    }

    IEnumerator ActionDelay() {
        possiblity = false;
        yield return new WaitForSeconds(transform.parent.GetComponent<PlayerAction>().GetPetReload());
        possiblity = true;
    }
}
