﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

// todo - handle booster gage 
public class Booster : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    private PlayerMovement playerMovement;
    private float originalSpeed;
    // Start is called before the first frame update
    void Start() {
        playerMovement = GameManager.instance.character.GetComponent<PlayerMovement>();
        originalSpeed = playerMovement.speed;
    }

    public void OnPointerDown(PointerEventData eventData) {
        if (GameManager.instance.checkBooster) {
            playerMovement.speed = playerMovement.speed + playerMovement.speed * 0.6f;
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        playerMovement.speed = originalSpeed;
    }
}
