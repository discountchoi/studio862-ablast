﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Anker : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    private PlayerMovement playerMovement;

    // Start is called before the first frame update
    void Start() {
        playerMovement = GameManager.instance.character.GetComponent<PlayerMovement>();

    }

    public void OnPointerDown(PointerEventData eventData) {
        if (GameManager.instance.checkAnker) {
            playerMovement.anked = true;
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        playerMovement.anked = false;
    }
}
