﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAction : MonoBehaviour {
    public MuzzleAction muzzleAction;
    public GameObject shield;
    public GameObject leftPet;
    public GameObject rightPet;
    public Weapon currentWeapon;
    public enum Weapon { spread, explode, bounce, guided };
    private float petReload = 2.0f;


    public void TurnOnShield() {
        shield.SetActive(true);
    }

    public void SwitchWeapon(Weapon weapon) {
        if (currentWeapon == weapon) {
            muzzleAction.BulletUpgrade();
        } else {
            muzzleAction.SetWeapon(weapon);
            currentWeapon = weapon;
        }
    }

    public void TurnOnLeftPet() {
        leftPet.SetActive(true);
    }

    public void TurnOnRightPet() {
        rightPet.SetActive(true);
    }

    public void IncreaseAttackSpeed() {
        muzzleAction.AttackSppedUp();
    }

    public void TurnOnSatelite() {
        // todo
    }

    public bool HasShield() {
        return shield.activeSelf;
    }

    public bool HasPet() {
        return (leftPet.activeSelf || rightPet.activeSelf);
    }

    public void IncreasePetAttackSpeed() {
        petReload = petReload - petReload * 0.2f;
    }

    public float GetPetReload() {
        return petReload;
    }
}
