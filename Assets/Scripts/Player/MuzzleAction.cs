﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleAction : MonoBehaviour {
    public int bulletLevel = 1;
    public int attackSpeedLevel = 1;
    private bool muzzleAvailability = true;
    public IMuzzle muzzle;

    // Start is called before the first frame update
    void Start() {
        SetWeapon(PlayerAction.Weapon.spread);
    }

    // Update is called once per frame
    void Update() {
        if (muzzleAvailability) {
            muzzle.Fire(bulletLevel, transform);
            StartCoroutine("ActionDelay");
        }
    }

    public void SetWeapon(PlayerAction.Weapon weapon) {
        muzzle = DataManager.instance.muzzles[(int)weapon].GetComponent<IMuzzle>();
    }

    public void BulletUpgrade() {
        ++bulletLevel;
    }

    public void AttackSppedUp() {
        ++attackSpeedLevel;
    }
  
    IEnumerator ActionDelay() {
        muzzleAvailability = false;
        yield return new WaitForSeconds(muzzle.GetReloadDelay() - muzzle.GetReloadDelay() * (attackSpeedLevel - 1) * 0.1f);
        muzzleAvailability = true;
    }

    void OnDestroy() {
        muzzle.CleanUp();
    }
}
