﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuidedMissileMuzzle : MonoBehaviour, IMuzzle
{
    public GameObject bullet;
    private float reloadTime = 1.0f;
    private bool possiblity;
    private Transform form;

    public void Fire(int bulletLevel, Transform muzzleTransform) {
        SoundManager.instance.PlaySound(SoundManager.Sounds.MissileFire);
        form = muzzleTransform;
        for (int i=1; i <= bulletLevel; i++) {
            //Instantiate(bullet, muzzleTransform.position, muzzleTransform.rotation);
            Invoke("MakeBullet", 0.05f * i);
        }
    }

    public float GetReloadDelay() {
        return reloadTime;
    }

    public void MakeBullet() {
        Instantiate(bullet, form.position, form.rotation);
    }

    public void CleanUp() {
        CancelInvoke();
    }
}

