﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeMuzzle : MonoBehaviour, IMuzzle {
    public GameObject bullet;
    private float reloadTime = 1.5f;
    private float defaultScale = 0.3f;

    public void CleanUp() {
    }

    public void Fire(int bulletLevel, Transform muzzleTransform) {
        SoundManager.instance.PlaySound(SoundManager.Sounds.MissileFire);
        var missile = Instantiate(bullet, muzzleTransform.position, muzzleTransform.rotation);
        missile.GetComponent<MissileBullet>().SetScale(defaultScale * (1 + (bulletLevel - 1) * 0.5f));
    }
    
    public float GetReloadDelay() {
        return reloadTime;
    }
}
