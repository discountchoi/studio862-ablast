﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncedBullet : MonoBehaviour, IBullet
{
    private float speed = 20f;
    public int bounceCount;
    // Update is called once per frame
    void Update() {
        Move();
    }

    public void SetBounceCount(int num) {
        bounceCount = num;
    }

    public void Move() {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
    }

    void OnCollisionEnter2D(Collision2D col) {
        if (col.collider.tag == "Wall" && bounceCount-- > 0) {
            SoundManager.instance.PlaySound(SoundManager.Sounds.BallBounce);
            var XPos = Mathf.Sin((transform.rotation.eulerAngles.z) * Mathf.Deg2Rad);
            var YPos = Mathf.Cos((transform.rotation.eulerAngles.z) * Mathf.Deg2Rad);
            Vector2 inDirection = new Vector2(-XPos, YPos);
            inDirection = inDirection.normalized;
            var noramlVector = col.contacts[0].normal;

            Vector2 reflectedVector = Vector2.Reflect(inDirection, noramlVector);
            reflectedVector = reflectedVector.normalized;
            var angle = Vector2.SignedAngle(Vector2.up, reflectedVector);
            transform.rotation = Quaternion.Euler(0f, 0f, angle);
        }
    }

    public bool IsPierce() {
        return true;
    }
}
