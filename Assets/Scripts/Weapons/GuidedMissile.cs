﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuidedMissile : MonoBehaviour, IBullet {
    // Start is called before the first frame update
    private GameObject target;
    private float speed = 10f;
    private float distance = 0f;
    private Collider2D petCollider;

    // Update is called once per frame
    void Update() {
        Move();
    }

    void CalcualteDistance(Collider2D collision) {
        if (collision.tag == "Enemy") {
            var dis = Vector3.Distance(transform.position, collision.transform.position);
            if (dis < distance) {
                distance = dis;
                target = collision.gameObject;
            }
        }
    }

    public void Move() {
        if (target == null) {
            distance = float.MaxValue;
            var enemies = Physics2D.OverlapCircleAll(transform.position, 5f);
            foreach (Collider2D target in enemies) {
                CalcualteDistance(target);
            }
        }

        if (target != null) {
            var vec = target.transform.position - transform.position;
            var angle = Mathf.Atan2(vec.y, vec.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(angle - 90, Vector3.forward), Time.deltaTime * speed * 2);
            //transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
            transform.Translate(Vector3.up * Time.deltaTime * speed);
        } else {
            transform.Translate(Vector3.up * Time.deltaTime * speed);
        }
    }

    public bool IsPierce() {
        return false;
    }
}
