﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncedBulletMuzzle : MonoBehaviour, IMuzzle {
    public GameObject bullet;
    private float reloadTime = 1.0f;

    public float GetReloadDelay() {
        return reloadTime;
    }

    public void Fire(int bulletLevel, Transform muzzleTransform) {
        // intatiate default bullet 
        SoundManager.instance.PlaySound(SoundManager.Sounds.BigBulletFire);
        var bouncedBullet = Instantiate(bullet, muzzleTransform.position, muzzleTransform.rotation);
        bouncedBullet.GetComponent<BouncedBullet>().SetBounceCount(bulletLevel);
    }

    public void CleanUp() {
    }
}
