﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IBullet {
    public float speed = 15f;

    // Update is called once per frame
    void Update() {
        Move();
    }

    public void Move() {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
    }

    public bool IsPierce() {
        return false;
    }
}
