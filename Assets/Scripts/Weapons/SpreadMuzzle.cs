﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadMuzzle : MonoBehaviour, IMuzzle {
    public GameObject bullet;
    private float reloadTime = 0.6f;

    public float GetReloadDelay() {
        return reloadTime;
    }

    public void Fire(int bulletLevel, Transform muzzleTransform) {
        SoundManager.instance.PlaySound(SoundManager.Sounds.BulletFire);
        int bulletCount = bulletLevel * 2 - 2;
        float bulletAngle = 60f / bulletCount;

        // intatiate default bullet 
        Instantiate(bullet, muzzleTransform.position, muzzleTransform.rotation);

        for (int i = 1; i <= bulletCount / 2; i++) {
            Instantiate(bullet, muzzleTransform.position, Quaternion.Euler(0f, 0f, muzzleTransform.eulerAngles.z + bulletAngle * i));
            Instantiate(bullet, muzzleTransform.position, Quaternion.Euler(0f, 0f, muzzleTransform.eulerAngles.z - bulletAngle * i));
        }
    }

    public void CleanUp() {
    }
}

