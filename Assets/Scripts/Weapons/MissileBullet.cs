﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileBullet : MonoBehaviour, IBullet {
    public GameObject explosion;

    private float speed = 15f;
    private float explosionScale;

    // Start is called before the first frame update
    void Start() {
        Destroy(gameObject, 0.5f);
    }

    // Update is called once per frame
    void Update() {
        Move();
    }

    public void SetScale(float scale) {
        explosion.transform.localScale = Vector3.one * scale;
    }

    public void Move() {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
    }

    private void OnDestroy() {
        Explode();
    }

    private void Explode() {
        GameObject exlosion = Instantiate(explosion, transform.position, transform.rotation);
    }

    public bool IsPierce() {
        return false;
    }
}
