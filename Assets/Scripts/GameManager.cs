﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    // Start is called before the first frame update
    public static GameManager instance;
    public GameObject gameoverUI;
    public Text scoreText;
    public Text bestScoreText;
    public LevelSystem levelSystem;
    public GameObject character;
    public GameObject controlUI;
    public GameObject upgradeUI;
    public Button boosterButton;
    public Button ankerButton;
    public Button restartButton;
    public Button menuButton;
    public Text gameoverProgress;
    public Dictionary<string, Sprite> items;
    public bool checkBooster = false;
    public bool checkAnker = false;
    private int score = 0;
    private bool gameover = false;
    private PlayerAction playerAction;
    private GoogleAdsManager AdsManager;

    void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Debug.LogWarning("Game Manager already existed in scene");
            Destroy(gameObject);
        }
        character = Instantiate(DataManager.instance.characters[DataManager.instance.index], transform.position, Quaternion.identity);
        InitializeFeatures();
    }

    void Start() {
        score = 0;
        gameover = false;
        playerAction = character.GetComponent<PlayerAction>();
        AdsManager = GameObject.Find("AdMob").GetComponent<GoogleAdsManager>();
    }

    // Update is called once per frame
    void Update() {

    }

    public void InitializeFeatures() {
        items = new Dictionary<string, Sprite>();
        for (int i = 0; i < DataManager.instance.items.Length; i++) {
            items.Add(DataManager.instance.items[i].name, DataManager.instance.items[i].sprite);
        }
    }

    public bool isInFrame(float x, float y) {
        if ((x < 8.9 && x > -8.9) && (y < 5 && y > -5)) {
            return true;
        }

        return false;
    }

    public bool isGameover() {
        return gameover;
    }

    public int GetCurrentLevel() {
        return levelSystem.GetCurrentLevel();
    }

    public void OnScore(int newScore) {
        if (!gameover) {
            score += newScore;
            scoreText.text = "Score : " + score;
        }

        levelSystem.AddExp(newScore);
    }

    public void OnPlayerDead() {
        gameover = true;
        gameoverUI.SetActive(true);
        controlUI.SetActive(false);

        // leaderboard
        gameoverProgress.text = "waitng to report the score.";
        Social.ReportScore(score, GPGSIds.leaderboard_score, (bool success) => {
            if (success) {
                gameoverProgress.text = "report success";
                restartButton.interactable = true;
                menuButton.interactable = true;
                Debug.Log("Leaderboard report success");
            }
        });

        bestScoreText.gameObject.SetActive(true);
        bestScoreText.text = "SCORE : " + score;

        DestroyAllItems();
        DestroyAllEnemies();
        PlayCloudDataManager.Instance.SaveToCloud(DataManager.instance.GetJewel().ToString());
    }

    /**
     * Move to other script file.
     * 
     */
    private void DestroyAllItems() {
        GameObject[] items = GameObject.FindGameObjectsWithTag("Item");
        foreach (GameObject o in items) {
            Destroy(o);
        }
    }

    private void DestroyAllEnemies() {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject o in enemies) {
            IEnemyMoveMent movement = o.GetComponent<IEnemyMoveMent>();
            movement.Die(false, true);
        }
    }

    public void OnRestartButtonClicked() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnMenuButtonClicked() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void OnFeatureClick(Text nameText) {
        bool removeAfterSelected = false;
        if (nameText.text == "anker") {
            checkAnker = true;
            ankerButton.interactable = true;
            removeAfterSelected = true;
        } else if (nameText.text == "attackSpeed") {
            playerAction.IncreaseAttackSpeed();
        } else if (nameText.text == "shield") {
            playerAction.TurnOnShield();
            removeAfterSelected = true;
        } else if (nameText.text == "explode") {
            playerAction.SwitchWeapon(PlayerAction.Weapon.explode);
        } else if (nameText.text == "leftPet") {
            playerAction.TurnOnLeftPet();
            removeAfterSelected = true;
        } else if (nameText.text == "guided") {
            playerAction.SwitchWeapon(PlayerAction.Weapon.guided);
        } else if (nameText.text == "rightPet") {
            playerAction.TurnOnRightPet();
            removeAfterSelected = true;
        } else if (nameText.text == "satelite") {
            playerAction.TurnOnSatelite();
            print("satelite feature");
        } else if (nameText.text == "spread") {
            playerAction.SwitchWeapon(PlayerAction.Weapon.spread);
        } else if (nameText.text == "booster") {
            checkBooster = true;
            boosterButton.interactable = true;
            removeAfterSelected = true;
        } else if (nameText.text == "bounce") {
            playerAction.SwitchWeapon(PlayerAction.Weapon.bounce);
        } else if (nameText.text == "petAttackSpeed") {
            playerAction.IncreasePetAttackSpeed();
        }

        if (removeAfterSelected) {
            items.Remove(nameText.text);
        }

        DeactivateUpgradeUI();
        Time.timeScale = 1;
    }

    public void LoadNextStage() {
        DestroyAllEnemies();
        ActivateUpgradeUI();
    }

    IEnumerator DelayedActivation() {
        yield return new WaitForSecondsRealtime(0.5f);
        Time.timeScale = 0;
        upgradeUI.SetActive(true);
    }

    public void ActivateUpgradeUI() {
        if (!isGameover()) {
            StartCoroutine("DelayedActivation");
        }
    }

    public void DeactivateUpgradeUI() {
        if (upgradeUI.activeSelf) {
            upgradeUI.SetActive(false);
        }
    }

    public void OnAdButtonClicked() {
        SoundManager.instance.PlaySound(SoundManager.Sounds.Menu);
        AdsManager.AdsShow();
        upgradeUI.GetComponent<UpgradeUI>().DrawLotsFromDict();
    }

    public void OnJewelButtonClicked() {
        if (DataManager.instance.GetJewel() >= 100) {
            SoundManager.instance.PlaySound(SoundManager.Sounds.Menu);
            DataManager.instance.DecJewel(100);
            upgradeUI.GetComponent<UpgradeUI>().DrawLotsFromDict();
            PlayCloudDataManager.Instance.SaveToCloud(DataManager.instance.GetJewel().ToString());
        }
    }

    public void OnCloseButtonClicked() {
        print("Close Upgrade Panel");
        DeactivateUpgradeUI();
        Time.timeScale = 1;
    }
}
