// code reference: http://answers.unity3d.com/questions/894995/how-to-saveload-with-google-play-services.html		
// you need to import https://github.com/playgameservices/play-games-plugin-for-unity
using UnityEngine;
using System;
using System.Collections;
//gpg
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
//for encoding
using System.Text;
//for extra save ui
using UnityEngine.SocialPlatforms;
//for text, remove
using UnityEngine.UI;
public class PlayCloudDataManager : MonoBehaviour {

    private static PlayCloudDataManager instance;

    public static PlayCloudDataManager Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<PlayCloudDataManager>();

                if (instance == null) {
                    instance = new GameObject("PlayGameCloudData").AddComponent<PlayCloudDataManager>();
                }
            }

            return instance;
        }
    }

    public bool isProcessing {
        get;
        private set;
    }

    public string loadedData {
        get;
        private set;
    }

    private const string m_saveFileName = "game_save_data";
    public bool isAuthenticated {
        get {
            return Social.localUser.authenticated;
        }
    }

    private void ProcessCloudData(byte[] cloudData) {
        if (cloudData == null) {
            Debug.Log("No Data saved to the cloud");
            return;
        }

        string progress = BytesToString(cloudData);
        loadedData = progress;
    }

    public void LoadFromCloud(Action<string> afterLoadAction) {
        StartCoroutine(LoadFromCloudRoutin(afterLoadAction));
    }

    private IEnumerator LoadFromCloudRoutin(Action<string> loadAction) {
        isProcessing = true;
        Debug.Log("Loading game progress from the cloud.");
        DataManager.instance.GetErrorText().text = "waiting to auto login";
        while (isAuthenticated == false) {
            GoogleManager.Instance.Login();
            yield return new WaitForSeconds(1f);
        }

        ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution(
            m_saveFileName, //name of file.
            DataSource.ReadCacheOrNetwork,
            ConflictResolutionStrategy.UseLongestPlaytime,
            OnFileOpenToLoad);
        DataManager.instance.GetErrorText().text = "waiting to load cloud data";

        while (isProcessing) {
            yield return null;
        }

        loadAction.Invoke(loadedData);
    }

    public void SaveToCloud(string dataToSave) {
        StartCoroutine(SaveToCloudRoutin(dataToSave));
    }

    IEnumerator SaveToCloudRoutin(string dataToSave) {
        loadedData = dataToSave;
        isProcessing = true;

        while (isAuthenticated == false) {
            GoogleManager.Instance.Login();
            yield return new WaitForSeconds(1f);
        }
        ((PlayGamesPlatform)Social.Active).SavedGame.OpenWithAutomaticConflictResolution(m_saveFileName, DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, OnFileOpenToSave);

        while (isProcessing) {
            yield return null;
        }
    }


    private void OnFileOpenToSave(SavedGameRequestStatus status, ISavedGameMetadata metaData) {
        if (status == SavedGameRequestStatus.Success) {
            byte[] data = StringToBytes(loadedData);
            SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
            SavedGameMetadataUpdate updatedMetadata = builder.Build();
            ((PlayGamesPlatform)Social.Active).SavedGame.CommitUpdate(metaData, updatedMetadata, data, OnGameSave);
        } else {
            Debug.LogWarning("Error opening Saved Game" + status);
        }
    }

    private void OnFileOpenToLoad(SavedGameRequestStatus status, ISavedGameMetadata metaData) {
        if (status == SavedGameRequestStatus.Success) {
            ((PlayGamesPlatform)Social.Active).SavedGame.ReadBinaryData(metaData, OnGameLoad);
        } else {
            DataManager.instance.GetErrorText().text = "OnFileOpenToLoad fail";
            Debug.LogWarning("Error opening Saved Game" + status);
        }
    }

    private void OnGameLoad(SavedGameRequestStatus status, byte[] bytes) {
        if (status != SavedGameRequestStatus.Success) {
            Debug.LogWarning("Error Saving" + status);
            DataManager.instance.GetErrorText().text = "OnGameLoad fail";
        } else {
            ProcessCloudData(bytes);
        }

        isProcessing = false;
    }

    private void OnGameSave(SavedGameRequestStatus status, ISavedGameMetadata metaData) {
        if (status != SavedGameRequestStatus.Success) {
            Debug.LogWarning("Error Saving" + status);
        } else {
            Debug.LogWarning("save success");
        }

        isProcessing = false;
    }

    private byte[] StringToBytes(string stringToConvert) {
        return Encoding.UTF8.GetBytes(stringToConvert);
    }

    private string BytesToString(byte[] bytes) {
        return Encoding.UTF8.GetString(bytes);
    }
}
