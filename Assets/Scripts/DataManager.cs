﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Item {
    // change to enumeration to handle conditional statement?
    public string name;
    public Sprite sprite;
}

public class DataManager : MonoBehaviour {
    // Start is called before the first frame update
    public static DataManager instance;
    public GameObject[] characters;
    public GameObject jewelPanel;
    public GameObject[] muzzles;
    public Item[] items;

    // Extract from items for conditional option in Upgrade UI panel;
    public Item shield;
    public Item petAttackSpeed;
    public int index;
    private int jewels = 0;
    public bool loaded = false;

    void Awake() {
        if (instance == null) {
            instance = this;
            PlayCloudDataManager.Instance.LoadFromCloud((string dataToLoad) => {
                GetErrorText().text = "parsing";
                if (int.TryParse(dataToLoad, out int data)) {
                    instance.SetJewel(data);
                } else {
                    instance.SetJewel(0);
                }

                GetErrorText().text = "loaded";
                JewelUpdate();
                loaded = true;
            });
        } else {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    public GameObject GetHelpPanel() {
        return GameObject.Find("Main").gameObject.transform.Find("HelpPanel").gameObject;
    }

    public Text GetErrorText() {
        return GameObject.Find("Main").gameObject.transform.Find("ErrorText").GetComponent<Text>();
    }

    void Start() {
        JewelUpdate();
    }

    // Update is called once per frame
    void Update() {

    }

    void JewelUpdate() {
        jewelPanel.GetComponent<JewelPanel>().JewelTextUpdate(jewels);
    }
    public int GetJewel() {
        return jewels;
    }

    public void SetJewel(int cnt) {
        jewels = cnt;
        JewelUpdate();
    }

    public void IncJewel(int cnt) {
        jewels += cnt;
        JewelUpdate();
    }

    public void DecJewel(int cnt) {
        jewels -= cnt;
        JewelUpdate();
    }
}
