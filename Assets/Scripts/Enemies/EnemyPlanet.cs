﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPlanet : MonoBehaviour, IEnemyMoveMent {
    public GameObject piece;
    public GameObject explode;
    public float speed;
    private Vector3 direction;
    private float scale;
    private int pieceCount;
    private float randomAngleSeed;
    void Start() {
        SetUp();
    }

    // Update is called once per frame
    void Update() {
        if (!GameManager.instance.isGameover()) {
            Move();
        }
    }

    // Implementation of IEnemyMovement interface
    public void SetUp() {
        scale = Random.Range(0.3f, 1.0f);
        pieceCount = Random.Range(2, 4);
        randomAngleSeed = Random.Range(0f, 60.0f);

        if (transform.position.x < 0) {
            direction = Vector3.right;
        } else {
            direction = Vector3.left;
        }
        transform.localScale *= scale;
    }

    public void Die(bool playerKill, bool forced) {
        if (GameManager.instance.isInFrame(transform.position.x, transform.position.y) || forced) {
            Instantiate(explode, transform.position, Quaternion.identity);
            if (playerKill) {
                var angle = 360.0f / pieceCount;
                if (scale >= 0.5) {
                    for (int i = 0; i < pieceCount; i++) {
                        Instantiate(piece, transform.position, Quaternion.Euler(0f, 0f, i * angle + randomAngleSeed));
                    }
                }
                SoundManager.instance.PlaySound(SoundManager.Sounds.PlanetDie);
                GameManager.instance.OnScore(40);
            }
            Destroy(gameObject);
        }
    }


    public void Move() {
        transform.Translate(direction * speed * Time.deltaTime);
    }
}
