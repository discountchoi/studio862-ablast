﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy01P : MonoBehaviour, IEnemyMoveMent {
    public GameObject explode;
    private float speed;
    // Start is called before the first frame update
    void Start() {
        SetUp();
    }

    // Update is called once per frame
    void Update() {
        if (!GameManager.instance.isGameover()) {
            Move();
        }
    }

    // Implementation of IEnemyMovement interface
    public void SetUp() {
        speed = Random.Range(1.0f, 2.0f);
    }

    private void SpawnJewel() {
        if (!GameManager.instance.isGameover()) {
            Instantiate(Resources.Load("Prefabs/Jewel"), transform.position, Quaternion.identity);
        }
    }

    public void Die(bool playerKill, bool forced) {
        if (GameManager.instance.isInFrame(transform.position.x, transform.position.y) || forced) {
            Instantiate(explode, transform.position, Quaternion.identity);
            if (playerKill) {
                SpawnJewel();
                SoundManager.instance.PlaySound(SoundManager.Sounds.EnemyDie);
                GameManager.instance.OnScore(20);
            }
            Destroy(gameObject);
        }
    }


    public void Move() {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
    }
}
