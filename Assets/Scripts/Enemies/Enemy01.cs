﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy01 : MonoBehaviour, IEnemyMoveMent {
    public GameObject explode;
    public GameObject pilot;
    public GameObject Bullet;

    private Vector3 direction;
    private GameObject instantiatedBullet;
    public float speed;
    private int exp;
    private int score;
    private float realoadTime = 5f;
    private bool possibility = true;
    // Start is called before the first frame update
    void Start() {
        SetUp();
    }

    // Update is called once per frame
    void Update() {
        if (!GameManager.instance.isGameover()) {
            Move();
        }
    }

    // Implementation of IEnemyMovement interface
    public void SetUp() {
        if (transform.position.x < 0) {
            direction = Vector3.right;
        } else {
            direction = Vector3.left;
        }
    }

    public void Die(bool playerKill, bool forced) {
        if (GameManager.instance.isInFrame(transform.position.x, transform.position.y) || forced) {
            Instantiate(explode, transform.position, Quaternion.identity);
            if (playerKill) {
                if (!GameManager.instance.isGameover()) {
                    Instantiate(pilot, transform.position, Quaternion.Euler(0f, 0f, 0f));
                }
                SoundManager.instance.PlaySound(SoundManager.Sounds.ShipDie);
                GameManager.instance.OnScore(50);
            } else {
                if (instantiatedBullet != null) {
                    Destroy(instantiatedBullet);
                }
            }
            Destroy(gameObject);
        }

    }

    public void Move() {
        if (possibility) {
            instantiatedBullet = Instantiate(Bullet, transform.position, transform.rotation);
            StartCoroutine("ActionDelay");
        }

        transform.Translate(direction * speed * Time.deltaTime);
    }

    IEnumerator ActionDelay() {
        possibility = false;
        yield return new WaitForSeconds(realoadTime);
        possibility = true;
    }
}

