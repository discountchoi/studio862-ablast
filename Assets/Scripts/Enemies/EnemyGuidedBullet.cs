﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGuidedBullet : MonoBehaviour, IBullet {
    
    public float speed;
    private Vector3 vec;

    // Start is called before the first frame update
    void Start() {
        vec = (GameManager.instance.character.transform.position - transform.position).normalized;
    }

    // Update is called once per frame
    void Update() {
        Move();
    }

    public void Move() {
        transform.Translate(vec * Time.deltaTime * speed);
    }

    public bool IsPierce() {
        return false;
    }
}
