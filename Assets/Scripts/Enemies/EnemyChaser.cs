﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChaser : MonoBehaviour, IEnemyMoveMent {
    public GameObject explode;
    private float speed;

    // Start is called before the first frame update
    void Start() {
        SetUp();
    }

    // Update is called once per frame
    void Update() {
        Move();
    }

    public void Die(bool playerKill, bool forced) {
        if (GameManager.instance.isInFrame(transform.position.x, transform.position.y) || forced) {
            Instantiate(explode, transform.position, Quaternion.identity);
            if (playerKill) {
                SoundManager.instance.PlaySound(SoundManager.Sounds.EnemyDie);
                GameManager.instance.OnScore(150);
            }
            Destroy(gameObject);
        }
    }

    public void Move() {
        var vec = GameManager.instance.character.transform.position - transform.position;
        var angle = Mathf.Atan2(vec.y, vec.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(angle-180, Vector3.forward), Time.deltaTime * speed * 2);
        transform.Translate(Vector3.left * Time.deltaTime * speed);
    }

    public void SetUp() {
        speed = 2.0f;
    }
}
