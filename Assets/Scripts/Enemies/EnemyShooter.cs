﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooter : MonoBehaviour, IEnemyMoveMent {
    public GameObject explode;
    public GameObject bullet;

    private Vector3 direction;
    private GameObject[] instantiatedBullet = new GameObject[5];
    public float speed;
    private int exp;
    private int score;
    private float realoadTime = 5f;
    private bool possibility = true;
    private Quaternion originalRotations;
    // Start is called before the first frame update
    void Start() {
        SetUp();
    }

    // Update is called once per frame
    void Update() {
        if (!GameManager.instance.isGameover()) {
            Move();
        }
    }

    // Implementation of IEnemyMovement interface
    public void SetUp() {
        originalRotations = transform.rotation;
        if (transform.position.x < 0) {
            direction = Vector3.right;
        } else {
            direction = Vector3.left;
        }
    }

    public void Die(bool playerKill, bool forced) {
        if (GameManager.instance.isInFrame(transform.position.x, transform.position.y) || forced) {
            Instantiate(explode, transform.position, Quaternion.identity);
            if (playerKill) {
                SoundManager.instance.PlaySound(SoundManager.Sounds.ShipDie);
                GameManager.instance.OnScore(100);
            } else {
                for (int i = 0; i < instantiatedBullet.Length; i++) {
                    if (instantiatedBullet[i] != null) {
                        Destroy(instantiatedBullet[i]);
                    }
                }
            }
            Destroy(gameObject);
        }
    }

    public void Move() {
        var dis = Vector2.Distance(transform.position, GameManager.instance.character.transform.position);
        if (possibility && dis < 8) {
            var direction = (GameManager.instance.character.transform.position - transform.position).normalized;
            var angle = Vector2.SignedAngle(Vector2.down, direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, angle), 1f);
            float bulletAngle = 60f / 4;
            Quaternion rotation = Quaternion.Euler(0f, 0f, angle + 180);

            // intatiate default bullet 
            instantiatedBullet[0] = Instantiate(bullet, transform.position, rotation);

            for (int i = 1; i <= 2; i++) {
                instantiatedBullet[i * 2 - 1] = Instantiate(bullet, transform.position, Quaternion.Euler(0f, 0f, rotation.eulerAngles.z + bulletAngle * i));
                instantiatedBullet[i * 2] = Instantiate(bullet, transform.position, Quaternion.Euler(0f, 0f, rotation.eulerAngles.z - bulletAngle * i));
            }
            StartCoroutine("ActionDelay");
        } else {
            transform.rotation = Quaternion.Lerp(transform.rotation, originalRotations, 0.1f);
            transform.Translate(direction * speed * Time.deltaTime);
        }
    }

    IEnumerator ActionDelay() {
        possibility = false;
        yield return new WaitForSeconds(realoadTime);
        possibility = true;
    }
}
