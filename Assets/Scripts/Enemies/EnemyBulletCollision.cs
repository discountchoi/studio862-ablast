﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletCollision : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "DeadZone") {
            Destroy(gameObject);
        } else if (collision.tag == "Shield") {
            GameManager.instance.character.transform.Find("Shield").gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }
}