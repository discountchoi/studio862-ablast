﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPlanetPiece : MonoBehaviour, IEnemyMoveMent {
    public GameObject explode;
    public float speed;
    private int exp;
    private int score;

    // Start is called before the first frame update
    void Start() {
        SetUp();
    }

    // Update is called once per frame
    void Update() {
        if (!GameManager.instance.isGameover()) {
            Move();
        }
    }

    // Implementation of IEnemyMovement interface
    public void SetUp() {
    }

    public void Die(bool playerKill, bool forced) {
        if (GameManager.instance.isInFrame(transform.position.x, transform.position.y) || forced) {
            Instantiate(explode, transform.position, Quaternion.identity);
            if (playerKill) {
                SoundManager.instance.PlaySound(SoundManager.Sounds.PlanetPieceDie);
                GameManager.instance.OnScore(20);
            }
            Destroy(gameObject);
        }

    }

    public void Move() {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
    }
}