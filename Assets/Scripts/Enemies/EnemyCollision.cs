﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollision : MonoBehaviour {
    private IEnemyMoveMent enemyMoveMent;
    // Start is called before the first frame update
    void Start() {
        enemyMoveMent = transform.GetComponent<IEnemyMoveMent>();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "DeadZone" || collision.tag == "Bullet") {
            if (collision.tag == "Bullet") {
                enemyMoveMent.Die(true, false);
            } else {
                Destroy(gameObject);
            }
        } else if (collision.tag == "Shield") {
            GameManager.instance.character.transform.Find("Shield").gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }
}
