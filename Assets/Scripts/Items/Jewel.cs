﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jewel : MonoBehaviour, IItem {
    public void Use(GameObject obj) {
        SoundManager.instance.PlaySound(SoundManager.Sounds.Jewel);
        DataManager.instance.IncJewel(1);
        Destroy(gameObject);
    }

    void Start() {
        Destroy(gameObject, 2.0f);
    }

    void Update() {
        //transform.Translate(GameManager.instance.character.transform.position);
        transform.position = Vector3.MoveTowards(transform.position, GameManager.instance.character.transform.position, Time.deltaTime * 0.5f);
        //print(GameManager.instance.character.transform.position);
    }
}
