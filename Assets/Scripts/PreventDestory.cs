﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreventDestory : MonoBehaviour
{
    public static PreventDestory instance;
    private void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
}
