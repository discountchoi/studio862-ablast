﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyMoveMent
{
    void SetUp();
    void Die(bool playerKill, bool forced);
    void Move();
}
