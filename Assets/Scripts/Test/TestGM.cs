﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGM : MonoBehaviour
{
    public static TestGM instance;
    void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Debug.LogWarning("Game Manager already existed in scene");
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnLeft() {
        print("Left");
    }

    public void OnRight() {
        print("Right");
    }
}
