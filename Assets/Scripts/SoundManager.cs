﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public enum Sounds { Menu, Player, Feature, Upgrade, Jewel, BulletFire, MissileFire, BigBulletFire, Explode, EnemyDie, ShipDie, PlanetDie, PlanetPieceDie, PlayerDie, PlayerBoost, BallBounce};
    public static SoundManager instance;
    public AudioClip MenuSelectClip;
    public AudioClip PlayerSelectClip;
    public AudioClip FeatureSelectClip;
    public AudioClip UpgradePanelClip;
    public AudioClip JewelClip;
    public AudioClip BulletFireClip;
    public AudioClip MissileFireClip;
    public AudioClip BigBulletFireClip;
    public AudioClip ExplodeClip;
    public AudioClip EnemyDieClip;
    public AudioClip ShipDieClip;
    public AudioClip PlanetDieClip;
    public AudioClip PlanetPieceClip;
    public AudioClip PlayerDieClip;
    public AudioClip PlayerBoostClip;
    public AudioClip BallBounceClip;

    private void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    public AudioClip GetSounds(Sounds sound) {
        AudioClip clip = null;
        switch (sound) {
            case Sounds.Menu:
                clip = MenuSelectClip;
                break;
            case Sounds.Player:
                clip = PlayerSelectClip;
                break;
            case Sounds.Feature:
                clip = FeatureSelectClip;
                break;
            case Sounds.Upgrade:
                clip = UpgradePanelClip;
                break;
            case Sounds.Jewel:
                clip = JewelClip;
                break;
            case Sounds.BulletFire:
                clip = BulletFireClip;
                break;
            case Sounds.MissileFire:
                clip = MissileFireClip;
                break;
            case Sounds.BigBulletFire:
                clip = BigBulletFireClip;
                break;
            case Sounds.Explode:
                clip = ExplodeClip;
                break;
            case Sounds.EnemyDie:
                clip = EnemyDieClip;
                break;
            case Sounds.ShipDie:
                clip = ShipDieClip;
                break;
            case Sounds.PlanetDie:
                clip = PlanetDieClip;
                break;
            case Sounds.PlanetPieceDie:
                clip = PlanetPieceClip;
                break;
            case Sounds.PlayerDie:
                clip = PlayerDieClip;
                break;
            case Sounds.PlayerBoost:
                clip = PlayerBoostClip;
                break;
            case Sounds.BallBounce:
                clip = BallBounceClip;
                break;
        }
        return clip;
    }

    public void PlaySound(Sounds sound) {
        var clip = GetSounds(sound);
        if (clip != null) {
            PlaySoundOnce(clip);
        }
    }

    public void PlaySoundOnce(AudioClip clip) {
        var source = GetComponent<AudioSource>();
        source.PlayOneShot(clip);
    }
}

