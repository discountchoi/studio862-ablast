﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMuzzle {
    void Fire(int bulletLevel, Transform muzzleTransform);
    float GetReloadDelay();
    void CleanUp();
}
