﻿using System;
using UnityEngine;
using GoogleMobileAds.Api;
using System.Collections;

public class GoogleAdsManager : MonoBehaviour {
    private InterstitialAd interstitial;

    // Start is called before the first frame update
    void Start() {
        MobileAds.Initialize((status) => { Debug.Log(status.ToString()); });
        RequestInterstitial();
    }

    private void RequestInterstitial() {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-1274745849416408/3764053525";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-1274745849416408/3764053525";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);

        // Called when an ad request has successfully loaded.
        interstitial.OnAdLoaded += HandleOnAdLoaded;

        // Called when an ad request failed to load.
        interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;

        // Called when an ad is shown.
        interstitial.OnAdOpening += HandleOnAdOpened;

        // Called when the ad is closed.
        interstitial.OnAdClosed += HandleOnAdClosed;

        // Called when the ad click caused the user to leave the application.
        interstitial.OnAdLeavingApplication += HandleOnAdLeavingApplication;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }

    public void HandleOnAdLoaded(object sender, EventArgs args) {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args) {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args) {
        MonoBehaviour.print("HandleAdClosed event received");
        interstitial.Destroy();
        RequestInterstitial();
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args) {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }

    private IEnumerator AdShowRoutine() {
        while(!interstitial.IsLoaded()) {
            yield return null;
        }
        interstitial.Show();
    }

    public void AdsShow() {
        StartCoroutine("AdShowRoutine");
    }
}
